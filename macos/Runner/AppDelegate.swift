import Cocoa
import FlutterMacOS
import flutter_local_notifications

@NSApplicationMain
class AppDelegate: FlutterAppDelegate {
  override func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
    // FlutterLocalNotificationsPlugin.setPluginRegistrantCallback { (registry) in
    //   GeneratedPluginRegistrant.register(with: registry)
    // }
    // GeneratedPluginRegistrant.register(with: self)
   
    return true
  }
}
