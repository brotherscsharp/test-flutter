# testflutter

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


# Open simulator
## iOS
xcrun simctl list
open -a Simulator --args -CurrentDeviceUDID <your device UDID>
or 
open -a Simulator

## Android
emulator -list-avds

emulator @pixel_5_-_api_29


# Run test
flutter drive --target=test_driver/TEST_NAME.dart

# Generate code
dart run build_runner build
flutter pub run build_runner build