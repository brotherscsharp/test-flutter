// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:namer_app/course/bloc/repository/lyrics_repository.dart';
import 'package:namer_app/course/block-counter/app.dart';
import 'package:namer_app/course/charts.dart';
import 'package:namer_app/course/notification.dart';
import 'package:namer_app/course/photo_filter.dart';
import 'package:namer_app/course/photo_list_paralax.dart';
import 'package:namer_app/course/testing/integration-list.dart';
import 'package:namer_app/course/testing/integration.dart';
import 'package:namer_app/course/theme_course.dart';
import 'package:namer_app/services/service_locator.dart';
import 'package:provider/provider.dart';
import 'course/app-state-course-second.dart';
import 'course/app-state-course.dart';
import 'course/bloc/lyrics-app.dart';
import 'course/bloc/model/mapper/song_mapper.dart';
import 'course/bloc/repository/local_client.dart';
import 'course/bloc/repository/lyrics_client.dart';
import 'course/block-counter/counter_observer.dart';
import 'course/custom-scroll-view.dart';
import 'course/fonts.dart';
import 'course/forms.dart';
import 'course/gestures.dart';
import 'course/handle-text-changes.dart';
import 'course/images.dart';
import 'course/lists.dart';
import 'course/local-storage.dart';
import 'course/long-list.dart';
import 'course/navigation.dart';
import 'course/networking-web-socket.dart';
import 'course/networking.dart';
import 'course/provider-future.dart';
import 'course/provider-multi.dart';
import 'course/provider-proxy-provider.dart';
import 'course/provider-stream.dart';
import 'course/provider-value-notifier.dart';
import 'course/provider.dart';
import 'course/read-write-file.dart';
import 'course/sqlite.dart';
import 'course/tabs.dart';
import 'course/screen-orientation.dart';
import 'course/liked-words.dart';
import 'course/adaptive-layout.dart';
import 'course/todo-list.dart';
import 'package:bloc/bloc.dart';

Future<void> main() async {
  final LyricsRepository lyricsRepository =
      LyricsRepository(LyricsClient(songMapper: SongMapper()), LocalClient());

  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  setupServiceLocator();

  // runApp(CustomScrollViewApp());
  // runApp(LongListApp());
  // runApp(NavigationApp());
  // runApp(MaterialApp.router(routerConfig: router)); // deep linking
  // runApp(TodosScreen()); // continue navigation course
  // runApp(NerworkingCourse()); // networking
  // runApp(WebSocketCourse()); // networking. Web Sockets
  // runApp(LocalStorageCourse());
  // runApp(ReadWriteCourse());
  // runApp(SqLiteCourse());
  // runApp(AppStateCourseSecond());
  // runApp(ProviderApp());
  // runApp(FutureProviderApp());
  // runApp(StreamProviderApp());
  // runApp(ValueListenableProviderApp());
  // runApp(MultiProviderApp());
  // runApp(ProxyProviderApp());
  // runApp(NotificationApp());
  // runApp(ChartsApp());
  // runApp(ThemeCourse());
  // return runApp(ChangeNotifierProvider<ThemeNotifier>(
  //   create: (_) => new ThemeNotifier(),
  //   child: ThemeCourse(),
  // ));
  // runApp(
  //   const MaterialApp(
  //     home: PhotoFilterCourse(),
  //     debugShowCheckedModeBanner: false,
  //   ),
  // );

  runApp(const PhotoListParalax());
  // runApp(
  //   EasyLocalization(
  //     path: 'assets/translations',
  //     supportedLocales: [Locale('en')],
  //     fallbackLocale: Locale('en'),
  //     child: LyricsApp(lyricsRepository: lyricsRepository),
  //   ),
  // );

  // Bloc.observer = const CounterObserver();
  // runApp(const CounterApp());

  // runApp(IntegrationListApp());
  // runApp(SongsApp());

  // runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => MyAppState(),
      child: MaterialApp(
        title: 'Name App',
        theme: ThemeData(
          useMaterial3: true,
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        ),
        home: ThemeCourse(),
      ),
    );
  }
}

final router = GoRouter(
  routes: [
    GoRoute(
      path: '/',
      builder: (_, __) => Scaffold(
        appBar: AppBar(
            title: Text(
          'Home Screen',
          style: TextStyle(
              fontFamily: 'Comforter', fontSize: 24, color: Colors.black),
        )),
      ),
      routes: [
        GoRoute(
          path: 'details',
          builder: (_, __) => Scaffold(
            appBar: AppBar(title: Text('Details Screen')),
          ),
        ),
      ],
    ),
  ],
);
