// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:namer_app/course/people.dart';

class AdaptiveLayoutCourse extends StatelessWidget {
  const AdaptiveLayoutCourse({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Namer App',
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyAdaptiveApp(),
    );
  }
}

class MyAdaptiveApp extends StatefulWidget {
  @override
  State<MyAdaptiveApp> createState() => _MyAdaptiveAppState();
}

class _MyAdaptiveAppState extends State<MyAdaptiveApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Adaptive app"),
      ),
      body: LayoutBuilder(builder: (context, constraints) {
        if (constraints.maxWidth > 450) {
          return WideLayout();
        } else {
          return NarrowLayout();
        }
      }),
    );
  }
}

class WideLayout extends StatefulWidget {
  @override
  State<WideLayout> createState() => _WideLayoutState();
}

class _WideLayoutState extends State<WideLayout> {
  Person? _person;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 250,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: PeopleList(onPersonTap: (person) {
              setState(() {
                _person = person;
              });
            }),
          ),
        ),
        Expanded(
          flex: 3,
          child: _person == null
              ? Placeholder()
              : PersonDetails(_person as Person),
        ),
      ],
    );
  }
}

class NarrowLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PeopleList(
        onPersonTap: (person) => Navigator.of(context).push(MaterialPageRoute(
            builder: (contexst) => Scaffold(
                  appBar: AppBar(),
                  body: PersonDetails(person),
                ))));
  }
}

class PeopleList extends StatelessWidget {
  final void Function(Person) onPersonTap;

  const PeopleList({required this.onPersonTap});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        for (var person in people)
          ListTile(
            // leading: Image.network(person.picture),
            title: Text(person.name),
            onTap: () => onPersonTap(person),
          )
      ],
    );
  }
}

class PersonDetails extends StatelessWidget {
  final Person person;
  const PersonDetails(this.person);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        if (constraints.maxHeight > 200) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(person.name),
                Text(person.phone),
                ElevatedButton(
                  onPressed: () =>
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text('Yay! ${person.name}'),
                  )),
                  child: Text('Looks like a RaisedButton'),
                ),
              ],
            ),
          );
        } else {
          return Center(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(person.name),
                  Text(person.phone),
                  ElevatedButton(
                    onPressed: () =>
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text('Yay! ${person.name}'),
                    )),
                    child: Text('Looks like a RaisedButton'),
                  ),
                ]),
          );
        }
      },
    );
  }
}
