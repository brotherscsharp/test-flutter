import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageCourse extends StatelessWidget {
  const LocalStorageCourse({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Namer App',
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LocalStorageApp(),
    );
  }
}

class LocalStorageApp extends StatefulWidget {
  @override
  State<LocalStorageApp> createState() => _LocalStorageAppState();
}

class _LocalStorageAppState extends State<LocalStorageApp> {
  final TextEditingController _controller = TextEditingController();
  String? dataFromStorage;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("LocalStorage App"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Form(
              child: TextFormField(
                controller: _controller,
                decoration: const InputDecoration(labelText: 'Send a message'),
              ),
            ),
            ElevatedButton(
              child: const Text('Save Data'),
              onPressed: () {
                saveData();
              },
            ),
            ElevatedButton(
              child: const Text('Read Data'),
              onPressed: () {
                readData();
              },
            ),
            ElevatedButton(
              child: const Text('Delete Data'),
              onPressed: () {
                deleteData();
              },
            ),
            Text(dataFromStorage ?? ''),
          ],
        ),
      ),
    );
  }

  Future<void> saveData() async {
    if (_controller.text.isNotEmpty) {
      final prefs = await SharedPreferences.getInstance();
      await prefs.setString('myString', _controller.text);
      _controller.text = '';
    }
  }

  Future<void> readData() async {
    final prefs = await SharedPreferences.getInstance();
    final myString = prefs.getString('myString') ?? '';
    setState(() {
      dataFromStorage = myString;
    });
  }

  Future<void> deleteData() async {
    final prefs = await SharedPreferences.getInstance();

    await prefs.remove('myString');
    await readData();
  }
}
