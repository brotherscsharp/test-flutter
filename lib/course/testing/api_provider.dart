import 'package:http/http.dart' show Client;
import 'dart:convert';

class ApiProvider {
  Client client = Client();
  fetchPosts() async {
    final response = await client
        .get(Uri.parse("https://jsonplaceholder.typicode.com/posts/1"));
    ItemModel itemModel = ItemModel.fromJson(json.decode(response.body));
    return itemModel;
  }
}

class ItemModel {
  int? _userId;
  int? _id;
  String? _title;
  String? _body;

  ItemModel.fromJson(Map<String, dynamic> parsedJson) {
    _userId = parsedJson['userId'];
    _id = parsedJson['id'];
    _title = parsedJson['title'];
    _body = parsedJson['body'];
  }

  String? get body => _body;

  String? get title => _title;

  int? get id => _id;

  int? get userId => _userId;
}
