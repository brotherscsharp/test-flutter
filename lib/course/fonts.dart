// import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';

class CustomFontCourse extends StatelessWidget {
  const CustomFontCourse({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Namer App',
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: CustomFontApp(),
    );
  }
}

class CustomFontApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Custom texts app"),
      ),
      body: Center(
        child: Column(children: [
          Text(
            'Hello Comforter font',
            style: TextStyle(fontFamily: 'Comforter', fontSize: 24),
            // style: GoogleFonts.lato(
            //   // textStyle: Theme.of(context).textTheme.headline4,
            //   fontStyle: FontStyle.italic,
            // ),
          ),
          Text(
            'Hello Kablammo font',
            style: TextStyle(fontFamily: 'Kablammo', fontSize: 24),
          ),
          Text(
            'Hello Oswald font',
            style: TextStyle(
                fontFamily: 'Oswald',
                fontSize: 24,
                fontWeight: FontWeight.bold),
          ),
          Text(
            'Hello Oswald font',
            style: TextStyle(
                fontFamily: 'Oswald',
                fontSize: 24,
                fontWeight: FontWeight.w800),
          ),
          Text(
            'Hello Oswald font',
            style: TextStyle(
                fontFamily: 'Oswald',
                fontSize: 24,
                fontWeight: FontWeight.w600),
          ),
          Text(
            'Hello Oswald font',
            style: TextStyle(
                fontFamily: 'Oswald',
                fontSize: 24,
                fontWeight: FontWeight.w400),
          ),
          Text(
            'Hello Oswald font',
            style: TextStyle(
                fontFamily: 'Oswald',
                fontSize: 24,
                fontWeight: FontWeight.w200),
          ),
          Text(
            'Hello Oswald font',
            style: TextStyle(
                fontFamily: 'Oswald',
                fontSize: 24,
                fontWeight: FontWeight.w100),
          ),
        ]),
      ),
    );
  }
}
