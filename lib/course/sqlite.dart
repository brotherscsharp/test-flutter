import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:namer_app/course/models/dog.dart';
import 'package:sqflite/sqflite.dart';

class SqLiteCourse extends StatefulWidget {
  const SqLiteCourse({super.key});

  @override
  State<SqLiteCourse> createState() => _SqLiteCourseState();
}

class _SqLiteCourseState extends State<SqLiteCourse> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Namer App',
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SqLiteApp(),
    );
  }
}

class SqLiteApp extends StatefulWidget {
  @override
  State<SqLiteApp> createState() => _SqLiteAppState();
}

class _SqLiteAppState extends State<SqLiteApp> {
  late TextEditingController dogName;
  late TextEditingController dogAge;
  late Database database;
  List<Dog> dogs = [];
  Dog? selectedDog;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SqLite App"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(children: [
              SizedBox(
                width: 200,
                child: TextFormField(
                  controller: dogName,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'name',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                ),
              ),
              SizedBox(
                width: 100,
                child: TextFormField(
                  controller: dogAge,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'age',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                ),
              ),
              Text(selectedDog == null ? '' : '${selectedDog?.id}'),
            ]),
            Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    insertDog();
                  },
                  child: const Text('Add'),
                ),
                ElevatedButton(
                  onPressed: () {
                    updateDog();
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  onPressed: () {
                    deleteDog();
                  },
                  child: const Text('Delete'),
                ),
                ElevatedButton(
                  onPressed: () {
                    getDogs();
                  },
                  child: const Text('Reload'),
                ),
              ],
            ),
            SizedBox(
              height: 350,
              child: ListView.builder(
                itemCount: dogs.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(dogs[index].name),
                    onTap: () => {
                      setState(() {
                        selectedDog = dogs[index];
                        dogName.text = selectedDog?.name ?? '';
                        dogAge.text =
                            selectedDog != null ? '${selectedDog?.age}' : '';
                      }),
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  int id = 0;
  Future<void> insertDog() async {
    // Get a reference to the database.
    // final db = await database;
    if (dogName.text.isEmpty && dogAge.text.isEmpty) {
      return;
    }
    id++;
    var dog = Dog(id: id, name: dogName.text, age: int.parse(dogAge.text));
    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await database.insert(
      'dogs',
      dog.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    clearFields();
    getDogs();
  }

  // / A method that retrieves all the dogs from the dogs table.
  Future<void> getDogs() async {
    // Get a reference to the database.
    // final db = await database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await database.query('dogs');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    var dogsList = List.generate(maps.length, (i) {
      return Dog(
        id: maps[i]['id'],
        name: maps[i]['name'],
        age: maps[i]['age'],
      );
    });

    setState(() {
      dogs = dogsList;
    });
  }

  Future<void> updateDog() async {
    // Get a reference to the database.
    // final db = await database;
    if (selectedDog?.id != null) {
      var dog = Dog(
          id: int.parse('${selectedDog?.id}'),
          name: dogName.text,
          age: int.parse(dogAge.text));
      // Update the given Dog.
      await database.update(
        'dogs',
        dog.toMap(),
        // Ensure that the Dog has a matching id.
        where: 'id = ?',
        // Pass the Dog's id as a whereArg to prevent SQL injection.
        whereArgs: [dog.id],
      );
      clearFields();
      getDogs();
    }
  }

  Future<void> deleteDog() async {
    // Remove the Dog from the database.
    if (selectedDog?.id != null) {
      await database.delete(
        'dogs',
        // Use a `where` clause to delete a specific dog.
        where: 'id = ?',
        // Pass the Dog's id as a whereArg to prevent SQL injection.
        whereArgs: ['${selectedDog?.id}'],
      );
      clearFields();
      getDogs();
    }
  }

  void clearFields() {
    dogName.text = '';
    dogAge.text = '';
    selectedDog = null;
  }

  @override
  void initState() {
    super.initState();
    dogName = TextEditingController();
    dogAge = TextEditingController();
    initDatabase();
  }

  @override
  void dispose() {
    database.close();
    super.dispose();
  }

  Future<void> initDatabase() async {
    var path = await getDatabasesPath();
    database = await openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      '$path/doggie_database.db',
      // When the database is first created, create a table to store dogs.
      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        return db.execute(
          'CREATE TABLE dogs(id INTEGER PRIMARY KEY, name TEXT, age INTEGER)',
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );
  }
}
