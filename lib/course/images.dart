import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class ImagesCourse extends StatelessWidget {
  const ImagesCourse({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Namer App',
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: ImagesApp(),
    );
  }
}

class ImagesApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle(
      style: Theme.of(context).textTheme.bodyMedium!,
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewportConstraints) {
          return Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: viewportConstraints.maxHeight,
                  ),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Fade in Transparent image file"),
                        FadeInImage.memoryNetwork(
                          placeholder: kTransparentImage,
                          image:
                              'https://www.bbva.com/wp-content/uploads/2018/07/gig-economy-freelance-economia-colaborativa-blockchain-bbva.jpg',
                        ),
                        Text("Local file"),
                        Image.asset(
                          'images/lake.jpg',
                          fit: BoxFit.fill,
                          width: 64,
                          height: 64,
                        ),
                        Text("Web file"),
                        Image.network('https://picsum.photos/250?image=9'),
                        Text("Web gif file"),
                        Image.network(
                          'https://docs.flutter.dev/assets/images/dash/dash-fainting.gif',
                          fit: BoxFit.fill,
                          errorBuilder: (context, error, stackTrace) {
                            print(error.toString());
                            return Text('some image here');
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
