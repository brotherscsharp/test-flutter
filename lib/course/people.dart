class Person {
  final String name;
  final String phone;
  final String picture;

  const Person(this.name, this.phone, this.picture);
}

final List<Person> people = _people
    .map((e) => Person(e['full_name'].toString(), e['phone'].toString(),
        e['picture'].toString()))
    .toList(growable: false);

final List<Map<String, Object>> _people = [
  {
    "id": 1,
    "name": "John",
    "last_name": "Doe",
    "full_name": "John Doe",
    "phone": "123-456-7890",
    "street": "123 Main Street",
    "about": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    "job": "Software Engineer",
    "picture":
        "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510_1280.jpg"
  },
  {
    "id": 2,
    "name": "Jane",
    "last_name": "Smith",
    "full_name": "Jane Smith",
    "phone": "987-654-3210",
    "street": "456 Elm Street",
    "about": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem.",
    "job": "Data Scientist",
    "picture":
        "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510_1280.jpg"
  },
  {
    "id": 3,
    "name": "Robert",
    "last_name": "Johnson",
    "full_name": "Robert Johnson",
    "phone": "555-123-4567",
    "street": "789 Oak Avenue",
    "about": "Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut.",
    "job": "Marketing Manager",
    "picture":
        "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510_1280.jpg"
  },
  {
    "id": 4,
    "name": "Emily",
    "last_name": "Williams",
    "full_name": "Emily Williams",
    "phone": "111-222-3333",
    "street": "222 Pine Street",
    "about":
        "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    "job": "Graphic Designer",
    "picture":
        "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510_1280.jpg"
  },
  {
    "id": 5,
    "name": "Michael",
    "last_name": "Brown",
    "full_name": "Michael Brown",
    "phone": "444-555-6666",
    "street": "333 Maple Avenue",
    "about":
        "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    "job": "Financial Analyst",
    "picture":
        "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510_1280.jpg"
  },
  {
    "id": 6,
    "name": "Olivia",
    "last_name": "Johnson",
    "full_name": "Olivia Johnson",
    "phone": "777-888-9999",
    "street": "444 Cedar Street",
    "about":
        "Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam.",
    "job": "Sales Manager",
    "picture":
        "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510_1280.jpg"
  },
];
