// handle-text-changes

import 'package:flutter/material.dart';

class HandleTextChangesCourse extends StatelessWidget {
  const HandleTextChangesCourse({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Namer App',
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HandleTextChangesApp(),
    );
  }
}

class HandleTextChangesApp extends StatefulWidget {
  @override
  State<HandleTextChangesApp> createState() => _HandleTextChangesAppState();
}

class _HandleTextChangesAppState extends State<HandleTextChangesApp> {
  final myController = TextEditingController();
  List<String> controllerList = [];
  List<String> eventHandlerList = [];

  @override
  void initState() {
    super.initState();

    // Start listening to changes.
    myController.addListener(_printLatestValue);
  }

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  void _printLatestValue() {
    setState(
      () {
        if (myController.text.isEmpty) {
          controllerList.clear();
        }
        controllerList.add(myController.text);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Handle Text Changes App"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            SizedBox(
              width: 250,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text('Controlle'),
                        TextFormField(
                          decoration: InputDecoration(hintText: "E-mail"),
                          controller: myController,
                        ),
                        SizedBox(
                          height: 250,
                          child: Column(
                            children: [
                              Expanded(
                                  child: ListView.builder(
                                      itemCount: controllerList.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return Text(controllerList[index]);
                                      })),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 250,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text('Event handler'),
                        TextField(
                          decoration: InputDecoration(hintText: "E-mail"),
                          onChanged: (text) {
                            setState(
                              () {
                                if (text.isEmpty) {
                                  eventHandlerList.clear();
                                }
                                eventHandlerList.add(text);
                              },
                            );
                          },
                        ),
                        SizedBox(
                          height: 250,
                          child: Column(
                            children: [
                              Expanded(
                                  child: ListView.builder(
                                      itemCount: eventHandlerList.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return Text(eventHandlerList[index]);
                                      })),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
