import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeCourse extends StatefulWidget {
  @override
  State<ThemeCourse> createState() => _ThemeCourseState();
}

class _ThemeCourseState extends State<ThemeCourse> {
  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeNotifier>(
        builder: (context, theme, child) => MaterialApp(
              theme: theme.getTheme(),
              home: Scaffold(
                appBar: AppBar(
                  title: Text('Hybrid Theme'),
                ),
                body: Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          child: ElevatedButton(
                            onPressed: () => {
                              print('Set Light Theme'),
                              theme.setLightMode(),
                            },
                            child: Text('Set Light Theme'),
                          ),
                        ),
                        Container(
                          child: ElevatedButton(
                            onPressed: () => {
                              print('Set Dark theme'),
                              theme.setDarkMode(),
                            },
                            child: Text('Set Dark theme'),
                          ),
                        ),
                      ],
                    ),
                    const Text('Some text'),
                    ElevatedButton(onPressed: () {}, child: Text('Button')),
                    ElevatedButton(
                        onPressed: null, child: Text('Disable Button')),
                  ],
                ),
              ),
            ));
  }
}

class ThemeNotifier with ChangeNotifier {
  final darkTheme = ThemeData(
    // primarySwatch: Colors.grey,
    // primaryColor: Colors.black,
    // brightness: Brightness.dark,
    // backgroundColor: const Color(0xFF212121),
    // // accentColor: Colors.white,
    // // accentIconTheme: IconThemeData(color: Colors.black),
    // dividerColor: Colors.black12,
    // secondaryHeaderColor: Colors.red,
    colorScheme: ColorScheme.fromSeed(
      brightness: Brightness.light,
      primary: Colors.yellow,
      onPrimary: Colors.black,
      // Colors that are not relevant to AppBar in LIGHT mode:
      secondary: Colors.grey,
      onSecondary: Colors.grey,
      background: Colors.grey,
      onBackground: Colors.grey,
      surface: Colors.grey,
      onSurface: Colors.grey,
      error: Colors.grey,
      onError: Colors.grey,
      seedColor: Colors.red,
    ),
  );

  final lightTheme = ThemeData(
    // primarySwatch: Colors.grey,
    // primaryColor: Colors.white,
    // brightness: Brightness.light,
    // backgroundColor: const Color(0xFFE5E5E5),
    // // accentColor: Colors.black,
    // // accentIconTheme: IconThemeData(color: Colors.white),
    // dividerColor: Colors.white54,
    colorScheme: ColorScheme.fromSeed(
      brightness: Brightness.light,
      primary: Colors.orange,
      onPrimary: Colors.red,
      // Colors that are not relevant to AppBar in LIGHT mode:
      secondary: Colors.green,
      onSecondary: Colors.blue,
      background: Colors.grey,
      onBackground: Colors.purple,
      surface: Colors.pink,
      onSurface: Colors.cyan,
      error: Colors.amber,
      onError: Colors.yellowAccent,
      seedColor: Colors.red,
    ),
  );

  late ThemeData _themeData;
  ThemeData getTheme() => _themeData;

  ThemeNotifier() {
    _themeData = lightTheme;
    StorageManager.readData('themeMode').then((value) {
      var themeMode = value ?? 'light';
      if (themeMode == 'light') {
        _themeData = lightTheme;
      } else {
        _themeData = darkTheme;
      }
      notifyListeners();
    });
  }

  void setDarkMode() async {
    _themeData = darkTheme;
    StorageManager.saveData('themeMode', 'dark');
    notifyListeners();
  }

  void setLightMode() async {
    _themeData = lightTheme;
    StorageManager.saveData('themeMode', 'light');
    notifyListeners();
  }
}

class StorageManager {
  static void saveData(String key, dynamic value) async {
    final prefs = await SharedPreferences.getInstance();
    if (value is int) {
      prefs.setInt(key, value);
    } else if (value is String) {
      prefs.setString(key, value);
    } else if (value is bool) {
      prefs.setBool(key, value);
    } else {
      print("Invalid Type");
    }
  }

  static Future<dynamic> readData(String key) async {
    final prefs = await SharedPreferences.getInstance();
    dynamic obj = prefs.get(key);
    return obj;
  }

  static Future<bool> deleteData(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.remove(key);
  }
}
