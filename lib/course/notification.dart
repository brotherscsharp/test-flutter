import 'package:flutter/material.dart';
import 'package:namer_app/services/notification_service/notification_service.dart';
import 'package:namer_app/services/service_locator.dart';

class NotificationApp extends StatefulWidget {
  const NotificationApp({Key? key}) : super(key: key);

  @override
  State<NotificationApp> createState() => _NotificationAppState();
}

class _NotificationAppState extends State<NotificationApp> {
  NotificationService _notificationService = getIt<NotificationService>();
  // FirebaseMessaging messaging = FirebaseMessaging.instance;

  @override
  void initState() {
    _notificationService.init(_onDidReceiveLocalNotification);
    // FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
    // requestPermissions();
    // handleMessages();
    super.initState();
  }

  // Future<void> requestPermissions() async {
  //   NotificationSettings settings = await messaging.requestPermission(
  //     alert: true,
  //     announcement: false,
  //     badge: true,
  //     carPlay: false,
  //     criticalAlert: false,
  //     provisional: false,
  //     sound: true,
  //   );
  //   print('User granted permission: ${settings.authorizationStatus}');
  // }

  // void handleMessages() {
  //   FirebaseMessaging.onMessage.listen((RemoteMessage message) {
  //     print('Got a message whilst in the foreground!');
  //     print('Message data: ${message.data}');

  //     if (message.notification != null) {
  //       print('Message also contained a notification: ${message.notification}');
  //     }
  //   });
  // }

  // Future<void> _firebaseMessagingBackgroundHandler(
  //     RemoteMessage message) async {
  //   // If you're going to use other Firebase services in the background, such as Firestore,
  //   // make sure you call `initializeApp` before using other Firebase services.
  //   // await Firebase.initializeApp();

  //   print("Handling a background message: ${message.messageId}");
  // }

  Future<dynamic> _onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) async {
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
                title: Text(title ?? ''),
                content: Text(body ?? ''),
                actions: [
                  TextButton(
                      child: Text("Ok"),
                      onPressed: () async {
                        _notificationService
                            .handleApplicationWasLaunchedFromNotification(
                                payload ?? '');
                      })
                ]));
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primaryColor: Colors.blue),
      home: NotificationScreen(),
    );
  }
}

class NotificationScreen extends StatefulWidget {
  @override
  State<NotificationScreen> createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  NotificationService _notificationService = getIt<NotificationService>();

  late TextEditingController _notificationMessage;

  @override
  void initState() {
    super.initState();
    _notificationMessage = TextEditingController();
    // setupInteractedMessage();
  }

  @override
  void dispose() {
    _notificationMessage.dispose();
    super.dispose();
  }

  // Future<void> setupInteractedMessage() async {
  //   // Get any messages which caused the application to open from
  //   // a terminated state.
  //   RemoteMessage? initialMessage =
  //       await FirebaseMessaging.instance.getInitialMessage();

  //   // If the message also contains a data property with a "type" of "chat",
  //   // navigate to a chat screen
  //   if (initialMessage != null) {
  //     _handleMessage(initialMessage);
  //   }

  //   // Also handle any interaction when the app is in the background via a
  //   // Stream listener
  //   FirebaseMessaging.onMessageOpenedApp.listen(_handleMessage);
  // }

  // void _handleMessage(RemoteMessage message) {
  //   if (message.data['type'] == 'chat') {
  //     Navigator.pushNamed(
  //       context,
  //       '/chat',
  //       arguments: message,
  //     );
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notifications app'),
      ),
      body: Column(
        children: <Widget>[
          TextFormField(
            controller: _notificationMessage,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Notification message',
              hintText: 'Enter text Here',
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter some text';
              }
              return null;
            },
          ),
          ElevatedButton(
            onPressed: () {
              _notificationService.showNotification(_notificationMessage.text);
            },
            child: const Text('send notification'),
          ),
        ],
      ),
    );
  }
}
