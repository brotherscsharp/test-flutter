import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:namer_app/course/bloc/feature/song/add_edit/bloc/song_add_edit_bloc.dart';
import 'package:namer_app/course/bloc/feature/song/search/bloc/songs_search_bloc.dart';
import 'package:namer_app/course/bloc/feature/song/search/ui/search_screen.dart';
import 'package:namer_app/course/bloc/repository/lyrics_repository.dart';
import 'package:namer_app/services/notification_service/notification_service.dart';
import 'package:namer_app/services/service_locator.dart';

class LyricsApp extends StatefulWidget {
  final LyricsRepository lyricsRepository;

  const LyricsApp({Key? key, required this.lyricsRepository}) : super(key: key);

  @override
  State<LyricsApp> createState() => _LyricsAppState();
}

class _LyricsAppState extends State<LyricsApp> {
  NotificationService _notificationService = getIt<NotificationService>();
  @override
  void initState() {
    _notificationService.init(_onDidReceiveLocalNotification);
    super.initState();
  }

  Future<dynamic> _onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) async {
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
                title: Text(title ?? ''),
                content: Text(body ?? ''),
                actions: [
                  TextButton(
                      child: Text("Ok"),
                      onPressed: () async {
                        // _notificationService.handleApplicationWasLaunchedFromNotification(payload ?? '');
                      })
                ]));
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<SongAddEditBloc>(
          create: (context) =>
              SongAddEditBloc(lyricsRepository: widget.lyricsRepository),
        ),
        BlocProvider<SongsSearchBloc>(
          create: (context) => SongsSearchBloc(
              lyricsRepository: widget.lyricsRepository,
              songAddEditBloc: BlocProvider.of<SongAddEditBloc>(context)),
        ),
      ],
      child: MaterialApp(
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        theme: ThemeData(primaryColor: Colors.blue),
        home: SearchScreen(),
      ),
    );
  }
}
