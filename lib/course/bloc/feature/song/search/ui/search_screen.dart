import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:namer_app/course/bloc/feature/song/add_edit/ui/song_add_edit_screen.dart';
import 'package:namer_app/course/bloc/feature/song/search/ui/search_bar.dart';
import 'package:namer_app/course/bloc/feature/song/search/ui/songs_search_list.dart';
import 'package:namer_app/course/bloc/resources/langs/strings.dart';
import 'package:namer_app/services/notification_service/notification_service.dart';
import 'package:namer_app/services/service_locator.dart';

class SearchScreen extends StatelessWidget {
  NotificationService _notificationService = getIt<NotificationService>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.APP_NAME.tr()),
      ),
      body: Column(
        children: <Widget>[
          ElevatedButton(
            onPressed: () {
              _notificationService.showNotification('Yahooo!');
              // Navigator.push(
              //   context,
              //   MaterialPageRoute(builder: (context) => SongAddScreen()),
              // );
            },
            child: const Text('send notification'),
          ),
          SearchBarCustom(),
          SongsSearchList()
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => SongAddScreen()),
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
