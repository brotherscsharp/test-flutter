// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:namer_app/course/bloc/feature/song/search/bloc/songs_search.dart';
// import 'package:namer_app/course/bloc/resources/langs/strings.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:namer_app/course/bloc/feature/song/search/bloc/songs_search.dart';
// import 'package:namer_app/course/bloc/feature/song/search/bloc/songs_search_event.dart';

class SearchBarCustom extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBarCustom> {
  final _songSearchController = TextEditingController();
  late SongsSearchBloc _songSearchBloc;

  @override
  void initState() {
    super.initState();
    _songSearchBloc = BlocProvider.of<SongsSearchBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _songSearchController,
      onChanged: (text) {
        _songSearchBloc.add(TextChanged(query: text));
      },
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.search),
          hintText: "Search lyrics" /*S.SEARCH_LYRICS.tr()*/),
    );
  }

  @override
  void dispose() {
    _songSearchController.dispose();
    super.dispose();
  }
}
