import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:namer_app/course/bloc/feature/song/add_edit/bloc/song_add_edit.dart';
import 'package:namer_app/course/bloc/feature/song/search/bloc/songs_search_event.dart';
import 'package:namer_app/course/bloc/feature/song/search/bloc/songs_search_state.dart';
import 'package:namer_app/course/bloc/model/api/search_result_error.dart';
import 'package:namer_app/course/bloc/model/domain/song_base.dart';
import 'package:namer_app/course/bloc/repository/lyrics_repository.dart';
import 'package:rxdart/rxdart.dart';

EventTransformer<SongSearchEvent> debounce<SongSearchEvent>(Duration duration) {
  return (events, mapper) => events.debounceTime(duration).flatMap(mapper);
}

class SongsSearchBloc extends Bloc<SongSearchEvent, SongsSearchState> {
  final LyricsRepository lyricsRepository;
  final SongAddEditBloc songAddEditBloc;

  late StreamSubscription addEditBlocSubscription;

  SongsSearchBloc({
    required this.lyricsRepository,
    required this.songAddEditBloc,
  }) : super(SearchStateEmpty()) {
    addEditBlocSubscription = songAddEditBloc.stream.listen((songAddEditState) {
      if (state is SearchStateSuccess) {
        if (songAddEditState is EditSongStateSuccess) {
          add(SongUpdated(song: songAddEditState.song));
        } else if (songAddEditState is AddSongStateSuccess) {
          add(SongAdded(song: songAddEditState.song));
        }
      }
    });
    on<TextChanged>(
        (event, emit) => _mapSongSearchTextChangedToState(event, emit),
        transformer: debounce(const Duration(milliseconds: 300)));

    on<SongAdded>((event, emit) => _mapSongAddedToState(event, emit),
        transformer: debounce(const Duration(milliseconds: 300)));
    on<SongUpdated>((event, emit) => _mapSongUpdateToState(event, emit),
        transformer: debounce(const Duration(milliseconds: 300)));
    on<RemoveSong>((event, emit) => _mapSongRemoveToState(event, emit),
        transformer: debounce(const Duration(milliseconds: 300)));
  }

  Future<void> _mapSongSearchTextChangedToState(TextChanged event, emit) async {
    final String searchQuery = event.query;
    if (searchQuery.isEmpty) {
      emit(SearchStateEmpty());
    } else {
      try {
        var result = await lyricsRepository.searchSongs(searchQuery);
        emit(SearchStateSuccess(result, searchQuery));
      } catch (error) {
        emit(error is SearchResultError
            ? SearchStateError(error.message)
            : SearchStateError("Default error"));
      }
    }
  }

  Future<void> _mapSongUpdateToState(SongUpdated event, emit) async {
    if (state is SearchStateSuccess) {
      SearchStateSuccess successState = state as SearchStateSuccess;
      List<SongBase> updatedList = successState.songs;
      if (event.song.isInQuery(successState.query)) {
        updatedList = updatedList.map((song) {
          return song.id == event.song.id ? event.song : song;
        }).toList();
      } else {
        updatedList.removeWhere((song) => song.id == event.song.id);
      }
      emit(SearchStateSuccess(updatedList, successState.query));
    }
  }

  Future<void> _mapSongAddedToState(SongAdded event, emit) async {
    if (state is SearchStateSuccess) {
      SearchStateSuccess successState = state as SearchStateSuccess;
      List<SongBase> updatedList = List.from(successState.songs);

      if (event.song.isInQuery(successState.query)) {
        updatedList.insert(0, event.song);
        emit(SearchStateSuccess(updatedList, successState.query));
      }
    }
  }

  Future<void> _mapSongRemoveToState(RemoveSong event, emit) async {
    await lyricsRepository.removeSong(event.songID);
    if (state is SearchStateSuccess) {
      SearchStateSuccess searchState = state as SearchStateSuccess;
      searchState.songs.removeWhere((song) {
        return song.id == event.songID;
      });
      emit(SearchStateSuccess(searchState.songs, searchState.query));
    }
  }

  @override
  Future<void> close() {
    addEditBlocSubscription.cancel();
    return super.close();
  }
}
