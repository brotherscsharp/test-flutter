import 'notification_service/notification_service_impl.dart';
import 'notification_service/notification_service.dart';

import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

setupServiceLocator() {
  getIt.registerLazySingleton<NotificationService>(
      () => NotificationServiceImpl());
}
