abstract class NotificationService {
  void init(
      Future<dynamic> Function(int, String?, String?, String?)? onDidReceive);
  void showNotification(String notificationMessage);
  void handleApplicationWasLaunchedFromNotification(String payload);
  void cancelNotificationForBirthday(int notificationId);
  void cancelAllNotifications();
  // Future selectNotification(String? payload);
  // void showNotification(UserBirthday userBirthday, String notificationMessage);
  // void scheduleNotificationForBirthday(UserBirthday userBirthday, String notificationMessage);
  // void scheduleNotificationForNextYear(UserBirthday userBirthday, String notificationMessage);
  // UserBirthday getUserBirthdayFromPayload(String payload);
  // Future<List<PendingNotificationRequest>> getAllScheduledNotifications();
}
