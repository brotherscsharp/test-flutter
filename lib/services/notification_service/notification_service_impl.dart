import 'dart:convert';
import 'dart:io' show Platform;
import 'dart:math';

import 'notification_service.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:timezone/data/latest.dart' as tz;

const String channel_id = "123";

class NotificationServiceImpl extends NotificationService {
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  void init(
      Future<dynamic> Function(int, String?, String?, String?)? onDidReceive) {
    final AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');

    final DarwinInitializationSettings initializationSettingsIOS =
        DarwinInitializationSettings(
            onDidReceiveLocalNotification: onDidReceive);

    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS,
            macOS: null);

    initializeLocalNotificationsPlugin(initializationSettings);

    tz.initializeTimeZones();
    _requestPermissions();
  }

  void initializeLocalNotificationsPlugin(
      InitializationSettings initializationSettings) async {
    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onDidReceiveNotificationResponse: selectNotification,
      // onDidReceiveBackgroundNotificationResponse: notificationTapBackground,
    );
    handleApplicationWasLaunchedFromNotification("");
  }

  // @pragma('vm:entry-point')
  // void notificationTapBackground(NotificationResponse notificationResponse) {
  //   // ignore: avoid_print
  //   print('notification(${notificationResponse.id}) action tapped: '
  //       '${notificationResponse.actionId} with'
  //       ' payload: ${notificationResponse.payload}');
  //   if (notificationResponse.input?.isNotEmpty ?? false) {
  //     // ignore: avoid_print
  //     print(
  //         'notification action tapped with input: ${notificationResponse.input}');
  //   }
  // }

  Future selectNotification(NotificationResponse payload) async {
    var von = payload;
    // do some staff here
  }

  @override
  void handleApplicationWasLaunchedFromNotification(String payload) {
    var von = payload;
    // do some staff here
  }

  @override
  Future<void> showNotification(String notificationMessage) async {
    const applicationName = 'Parrot Wings';
    var id = Random().nextInt(10000);
    const userBirthday = 'some details here';
    await flutterLocalNotificationsPlugin.show(
        id,
        applicationName,
        notificationMessage,
        const NotificationDetails(
            android: AndroidNotificationDetails(channel_id, applicationName,
                channelDescription: 'Some details')),
        payload: jsonEncode(userBirthday));
  }

  @override
  void cancelNotificationForBirthday(int notificationId) async {
    await flutterLocalNotificationsPlugin.cancel(notificationId);
  }

  @override
  void cancelAllNotifications() async {
    await flutterLocalNotificationsPlugin.cancelAll();
  }

  void _rescheduleNotificationFromPayload(String payload) {
    // UserBirthday userBirthday = getUserBirthdayFromPayload(payload);
    // cancelNotificationForBirthday(userBirthday);
    // scheduleNotificationForBirthday(userBirthday, "${userBirthday.name} has an upcoming birthday!");
  }

  // void handleApplicationWasLaunchedFromNotification(String payload) async {
  //   if (Platform.isIOS) {
  //     _rescheduleNotificationFromPayload(payload);
  //     return;
  //   }

  //   final NotificationAppLaunchDetails? notificationAppLaunchDetails =
  //       await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

  //   if (notificationAppLaunchDetails != null &&
  //       notificationAppLaunchDetails.didNotificationLaunchApp) {
  //     _rescheduleNotificationFromPayload(
  //         notificationAppLaunchDetails.notificationResponse?.payload ?? "");
  //   }
  // }

  // Future<bool> _wasApplicationLaunchedFromNotification() async {
  //   NotificationAppLaunchDetails? notificationAppLaunchDetails = await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

  //   if (notificationAppLaunchDetails != null) {
  //     return notificationAppLaunchDetails.didNotificationLaunchApp;
  //   }

  //   return false;
  // }

  Future<List<PendingNotificationRequest>>
      getAllScheduledNotifications() async {
    List<PendingNotificationRequest> pendingNotifications =
        await flutterLocalNotificationsPlugin.pendingNotificationRequests();
    return pendingNotifications;
  }

  Future<void> _requestPermissions() async {
    if (Platform.isIOS || Platform.isMacOS) {
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              IOSFlutterLocalNotificationsPlugin>()
          ?.requestPermissions(
            alert: true,
            badge: true,
            sound: true,
          );
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              MacOSFlutterLocalNotificationsPlugin>()
          ?.requestPermissions(
            alert: true,
            badge: true,
            sound: true,
          );
    } else if (Platform.isAndroid) {
      final AndroidFlutterLocalNotificationsPlugin? androidImplementation =
          flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>();

      final bool? grantedNotificationPermission =
          await androidImplementation?.requestPermission();
      // setState(() {
      //   _notificationsEnabled = grantedNotificationPermission ?? false;
      // });
    }
  }
}
