/* commented to hide errors
class nullSafety {
    int? stringLength(String? nullableString) {
  return nullableString?.length;
}


//---------------------
int? storedNullableValue;

  /// If [storedNullableValue] is currently `null`,
  /// set it to the result of [calculateValue] 
  /// or `0` if [calculateValue] returns `null`.
  void updateStoredValue() {
    storedNullableValue ??= calculateValue() ?? 0;
  }

  /// Calculates a value to be used,
  /// potentially `null`.
  int? calculateValue();

  //-----------------------

  int getLength(String? str) {
    // Add null check here

    return str?.length ?? 0;
    }

    void main() {
    print(getLength('This is a string!'));
    }
  //-----------------------


    void main() {
    late final myTeam = Team();
    late final myCoach = Coach();
    myTeam.coach = myCoach;
    myCoach.team = myTeam;

    print('All done!');
    }
}

class Team {
  late final Coach coach;
}

class Coach {
  late final Team team;
}
*/