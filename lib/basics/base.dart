/* commented to hide errors

// Collection literals

// Assign this a list containing 'a', 'b', and 'c' in that order:
final aListOfStrings = ['a', 'b', 'c'];

// Assign this a set containing 3, 4, and 5:
final aSetOfInts = {3, 4, 5};

// Assign this a map of String to int so that aMapOfStringsToInts['myKey'] returns 12:
final aMapOfStringsToInts = {'myKey': 12};

// Assign this an empty List<double>:
final anEmptyListOfDouble = <double>[];

// Assign this an empty Set<String>:
final anEmptySetOfString = <String>{};

// Assign this an empty Map of double to int:
final anEmptyMapOfDoublesToInts = <double, int>{};


//-------------------------------
  int value1 = 2;
  int value2 = 3;
  int value3 = 5;
  
  // Returns the product of the above values:
  int get product => value1 * value2 * value3;
  
  // Adds 1 to value1:
  void incrementValue1() => value1++;
  
  // Returns a string containing each item in the
  // list, separated by commas (e.g. 'a,b,c'): 
  String joinWithCommas(List<String> strings) => strings.join(',');

//--------------------------------

// Cascades
// Use cascades to create a single statement that sets the anInt, aString, and aList properties of a BigObject to 1, 'String!', and [3.0] (respectively) and then calls allDone().
class BigObject {
  int anInt = 0;
  String aString = '';
  List<double> aList = [];
  bool _done = false;
  
  void allDone() {
    _done = true;
  }
}

BigObject fillBigObject(BigObject obj) {
  // Create a single statement that will update and return obj:
  return obj..anInt = 1
    ..aString = 'String!'
    ..aList = [3.0]
    ..allDone();
}

//----------------------------

// Getter ans Setter
// Imagine you have a shopping cart class that keeps a private List<double> of prices. Add the following:
// A getter called total that returns the sum of the prices
// A setter that replaces the list with a new one, as long as the new list doesn’t contain any negative prices (in which case the setter should throw an InvalidPriceException).
class InvalidPriceException {}

class ShoppingCart {
  List<double> _prices = [];
  
  // Add a "total" getter here:
  double get total => _prices.fold(0, (e, t) => e + t);
  

  // Add a "prices" setter here:
  set prices(List<double> value) {
    if(value.any((l) => l < 0)) {
      throw InvalidPriceException();
    }
    _prices = value;
  }
}

//-----------------------
// Implement a function called joinWithCommas() that accepts one to five integers, then returns a string of those numbers separated by commas. Here are some examples of function calls and returned values:
String joinWithCommas(int a, [int? b, int? c, int? d, int? e]) {
  List<int?> params = [a, b, c, d, e];  
  List<int?> arr = params.where((r) => r != null).toList();
  return arr.join(',');
}


//------------------------
// Named parameters
// Add a copyWith() instance method to the MyDataObject class. It should take three named, nullable parameters:

// int? newInt
// String? newString
// double? newDouble
// Your copyWith() method should return a new MyDataObject based on the current instance, with data from the preceding parameters (if any) copied into the object’s properties. For example, if newInt is non-null, then copy its value into anInt.

class MyDataObject {
  final int anInt;
  final String aString;
  final double aDouble;

  MyDataObject({
     this.anInt = 1,
     this.aString = 'Old!',
     this.aDouble = 2.0,
  });

  MyDataObject copyWith({int? newInt, String? newString, double? newDouble}) {
    return MyDataObject(
      anInt: newInt ?? this.anInt,
      aString: newString ?? this.aString,
      aDouble: newDouble ?? this.aDouble,
    );
  }
}


////---------------

// Excaptions
// Implement tryFunction() below. It should execute an untrustworthy method and then do the following:

// If untrustworthy() throws an ExceptionWithMessage, call logger.logException with the exception type and message (try using on and catch).
// If untrustworthy() throws an Exception, call logger.logException with the exception type (try using on for this one).
// If untrustworthy() throws any other object, don’t catch the exception.
// After everything’s caught and handled, call logger.doneLogging (try using finally).

typedef VoidFunction = void Function();

class ExceptionWithMessage {
  final String message;
  const ExceptionWithMessage(this.message);
}

abstract class Logger {
  void logException(Type t, [String? msg]);
  void doneLogging();
}

void tryFunction(VoidFunction untrustworthy, Logger logger) {
  try {
    untrustworthy();
  } on ExceptionWithMessage catch (e) {
    logger.logException(e.runtimeType, e.message);
  } on Exception {
    logger.logException(Exception);
  } finally {
    logger.doneLogging();
  }
}

//----------------------------
// Complete the FirstTwoLetters constructor below. Use an initializer list to assign the first two characters in word to the letterOne and LetterTwo properties.
// For extra credit, add an assert to catch words of less than two characters.
class FirstTwoLetters {
  String? letterOne;
  String? letterTwo;

  // Create a constructor with an initializer list here:
  FirstTwoLetters(String word): assert(word.length >= 2) {
    letterOne = word[0];
    letterTwo = word[1];
  }
}

or original 
class FirstTwoLetters {
  final String letterOne;
  final String letterTwo;

  FirstTwoLetters(String word)
      : assert(word.length >= 2),
        letterOne = word[0],
        letterTwo = word[1];
}

//----------------------------
// Named constructors
// Give the Color class a constructor named Color.black that sets all three properties to zero..
class Color {
  int red;
  int green;
  int blue;
  
  Color(this.red, this.green, this.blue);

  // Create a named constructor called "Color.black" here:
  Color.black(): 
    red = 0,
    green = 0,
    blue = 0;
}

//----------------------------
// Factory constructors
// Fill in the factory constructor named IntegerHolder.fromList, making it do the following:

// If the list has one value, create an IntegerSingle with that value.
// If the list has two values, create an IntegerDouble with the values in order.
// If the list has three values, create an IntegerTriple with the values in order.
// Otherwise, throw an Error.

class IntegerHolder {
  IntegerHolder();
  
  // Implement this factory constructor.
  factory IntegerHolder.fromList(List<int> list) {
    if (list.length == 1) return IntegerSingle(list[0]);
    if (list.length == 2) return IntegerDouble(list[0], list[1]);
    if (list.length == 3) return IntegerTriple(list[0], list[1], list[2]);
    
    throw Error();
  }
}

class IntegerSingle extends IntegerHolder {
  final int a;
  IntegerSingle(this.a); 
}

class IntegerDouble extends IntegerHolder {
  final int a;
  final int b;
  IntegerDouble(this.a, this.b); 
}

class IntegerTriple extends IntegerHolder {
  final int a;
  final int b;
  final int c;
  IntegerTriple(this.a, this.b, this.c); 
}

//----------------------------
// Redirecting constructors
// Remember the Color class from above? Create a named constructor called black, but rather than manually assigning the properties, redirect it to the default constructor with zeros as the arguments.
class Color {
  int red;
  int green;
  int blue;
  
  Color(this.red, this.green, this.blue);

  // Create a named constructor called "black" here and redirect it
  // to call the existing constructor
  Color.black() : this(0, 0, 0);
}


//----------------------------
// Const constructors
// Modify the Recipe class so its instances can be constants, and create a constant constructor that does the following:

// Has three parameters: ingredients, calories, and milligramsOfSodium (in that order).
// Uses this. syntax to automatically assign the parameter values to the object properties of the same name.
// Is constant, with the const keyword just before Recipe in the constructor declaration.
class Recipe {
  final List<String> ingredients;
  final int calories;
  final double milligramsOfSodium;
  
  const Recipe(this.ingredients, this.calories, this.milligramsOfSodium);
}


//----------------------------

*/