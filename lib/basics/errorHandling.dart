/* commented to hide errors
class ErrorHandling {
    void tryCatch() {
        try {
            breedMoreLlamas();
        } on OutOfLlamasException {
            buyMoreLlamas();
        }
    }

    void catchExceptions() {
        try {
            breedMoreLlamas();
        } on OutOfLlamasException {
            // A specific exception
            buyMoreLlamas();
        } on Exception catch (e) {
            // Anything else that is an exception
            print('Unknown exception: $e');
        } catch (e) {
            // No specified type, handles all
            print('Something really unknown: $e');
        }
    }

    void catchWithCallStack() {
        try {
            // ···
        } on Exception catch (e) {
            print('Exception details:\n $e');
        } catch (e, s) {
            print('Exception details:\n $e');
            print('Stack trace:\n $s');
        }
    }

    void catchWithRethrow() {
        try {
            dynamic foo = true;
            print(foo++); // Runtime error
        } catch (e) {
            print('misbehave() partially handled ${e.runtimeType}.');
            rethrow; // Allow callers to see the exception.
        }
    }

    void catchWithFinally() {
        try {
            breedMoreLlamas();
        } catch (e) {
            print('Error: $e'); // Handle the exception first.
        } finally {
            cleanLlamaStalls(); // Then clean up.
        }
    }
}
*/