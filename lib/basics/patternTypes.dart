/* commented to hide errors
class PatternTypes {
    // Logical-or
    var isPrimary = switch (color) {
        Color.red || Color.yellow || Color.blue => true,
        _ => false
    };

    // Logical-and
    switch ((1, 2)) {
        // Error, both subpatterns attempt to bind 'b'.
        case (var a, var b) && (var b, var c): // ...
    }

    // Relational
    String asciiCharType(int char) {
        const space = 32;
        const zero = 48;
        const nine = 57;

        return switch (char) {
            < space => 'control',
            == space => 'space',
            > space && < zero => 'punctuation',
            >= zero && <= nine => 'digit',
            _ => ''
        };
    }

    // Cast
    (num, Object) record = (1, 's');
    var (i as int, s as String) = record;

    // Null-check
    String? maybeString = 'nullable with base type String';
    switch (maybeString) {
    case var s?:
    // 's' has type non-nullable String here.
    }

    // Null-assert
    List<String?> row = ['user', null];
    switch (row) {
        case ['user', var name!]: // ...
        // 'name' is a non-nullable string here.
    }

    // Constant
    // 123, null, 'string', math.pi, SomeClass.constant, const Thing(1, 2), const (1 + 2)
    switch (number) {
        // Matches if 1 == number.
        case 1: // ...
    }

    // Variable
    // var bar, String str, final int _
    switch ((1, 2)) {
        // 'var a' and 'var b' are variable patterns that bind to 1 and 2, respectively.
        case (var a, var b): // ...
        // 'a' and 'b' are in scope in the case body.
    }

    // Identifier
    // foo, _
    const c = 1;
    switch (2) {
        case c:
            print('match $c');
        default:
            print('no match'); // Prints "no match".
    }

    // Parenthesized
    // For example, imagine the boolean constants x, y, and z are equal to true, true, and false, respectively:
    x || y && z => 'matches true',
    (x || y) && z => 'matches false',

    // List
    const a = 'a';
    const b = 'b';
    switch (obj) {
    // List pattern [a, b] matches obj first if obj is a list with two fields,
    // then if its fields match the constant subpatterns 'a' and 'b'.
    case [a, b]:
        print('$a, $b');
    }

    // Rest element
    var [a, b, ..., c, d] = [1, 2, 3, 4, 5, 6, 7];
    // Prints "1 2 6 7".
    print('$a $b $c $d');

    var [a, b, ...rest, c, d] = [1, 2, 3, 4, 5, 6, 7];
    // Prints "1 2 [3, 4, 5] 6 7".
    print('$a $b $rest $c $d');


    // Map
    // {"key": subpattern1, someConst: subpattern2}

    // Record
    // var (myString: foo, myNumber: bar) = (myString: 'string', myNumber: 1);
    // Record pattern with variable subpatterns:
    var (untyped: untyped, typed: int typed) = record;
    var (:untyped, :int typed) = record;

    switch (record) {
    case (untyped: var untyped, typed: int typed): // ...
    case (:var untyped, :int typed): // ...
    }

    // Record pattern wih null-check and null-assert subpatterns:
    switch (record) {
    case (checked: var checked?, asserted: var asserted!): // ...
    case (:var checked?, :var asserted!): // ...
    }

    // Record pattern wih cast subpattern:
    var (untyped: untyped as int, typed: typed as String) = record;
    var (:untyped as int, :typed as String) = record;

    // Object 
    //SomeClass(x: subpattern1, y: subpattern2)
    switch (shape) {
        // Matches if shape is of type Rect, and then against the properties of Rect.
        case Rect(width: var w, height: var h): // ...
    }

    // Binds new variables x and y to the values of Point's x and y properties.
    var Point(:x, :y) = Point(1, 2);


    // Wildcard
    var list = [1, 2, 3];
    var [_, two, _] = list;


    switch (record) {
        case (int _, String _):
            print('First field is int and second is String.');
    }
}
*/