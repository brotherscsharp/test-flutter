/* commented to hide errors

class asyncCode {
    // Example: Execution within async functions
    Future<void> printOrderMessage() async {
    print('Awaiting user order...');
    var order = await fetchUserOrder();
    print('Your order is: $order');
    }

    Future<String> fetchUserOrder() {
    // Imagine that this function is more complex and slow.
    return Future.delayed(const Duration(seconds: 4), () => 'Large Latte');
    }

    void main() async {
    countSeconds(4);
    await printOrderMessage();
    }

    // You can ignore this function - it's here to visualize delay time in this example.
    void countSeconds(int s) {
    for (var i = 1; i <= s; i++) {
        Future.delayed(Duration(seconds: i), () => print(i));
    }
    }

    //--------------------------

    //     Add code to the reportUserRole() function so that it does the following:

    // Returns a future that completes with the following string: "User role: <user role>"
    // Note: You must use the actual value returned by fetchRole(); copying and pasting the example return value won’t make the test pass.
    // Example return value: "User role: tester"
    // Gets the user role by calling the provided function fetchRole().
    // Part 2: reportLogins()
    // Implement an async function reportLogins() so that it does the following:

    // Returns the string "Total number of logins: <# of logins>".
    // Note: You must use the actual value returned by fetchLoginAmount(); copying and pasting the example return value won’t make the test pass.
    // Example return value from reportLogins(): "Total number of logins: 57"
    // Gets the number of logins by calling the provided function fetchLoginAmount().
    
    // Part 1
    // You can call the provided async function fetchRole()
    // to return the user role.
    Future<String> reportUserRole() async {
    var role = await fetchRole();
        return 'User role: $role';
    }

    // Part 2
    // Implement reportLogins here
    // You can call the provided async function fetchLoginAmount()
    // to return the number of times that the user has logged in.
    reportLogins() async {
    var logins = await fetchLoginAmount();
        return 'Total number of logins: $logins';
    }
    //--------------------------

    // Use async and await to implement an asynchronous changeUsername() function that does the following:

    // Calls the provided asynchronous function fetchNewUsername() and returns its result.
    // Example return value from changeUsername(): "jane_smith_92"
    // Catches any error that occurs and returns the string value of the error.
    // You can use the toString() method to stringify both Exceptions and Errors.

    Future<String> changeUsername() async {
        try {
            return await fetchNewUsername();
        } catch (err) {
            return err.toString();
        }
    }
    //--------------------------
    //     Write the following:

    // Part 1: addHello()
    // Write a function addHello() that takes a single String argument.
    // addHello() returns its String argument preceded by 'Hello '.
    // Example: addHello('Jon') returns 'Hello Jon'.
    // Part 2: greetUser()
    // Write a function greetUser() that takes no arguments.
    // To get the username, greetUser() calls the provided asynchronous function fetchUsername().
    // greetUser() creates a greeting for the user by calling addHello(), passing it the username, and returning the result.
    // Example: If fetchUsername() returns 'Jenny', then greetUser() returns 'Hello Jenny'.
    // Part 3: sayGoodbye()
    // Write a function sayGoodbye() that does the following:
    // Takes no arguments.
    // Catches any errors.
    // Calls the provided asynchronous function logoutUser().
    // If logoutUser() fails, sayGoodbye() returns any string you like.
    // If logoutUser() succeeds, sayGoodbye() returns the string '<result> Thanks, see you next time', where <result> is the string value returned by calling logoutUser().

    // Part 1
    addHello(String user) {
        return 'Hello $user';
    }

    // Part 2
    // You can call the provided async function fetchUsername()
    // to return the username.
    greetUser() async {
        return addHello(await fetchUsername());
    }

    // Part 3
    // You can call the provided async function logoutUser()
    // to log out the user.
    sayGoodbye() async {
        try{
            var result = await logoutUser();
            return '$result Thanks, see you next time';
        } catch (error) {
            error.toString();
            return 'Oooops!';
        }
    }

}
*/