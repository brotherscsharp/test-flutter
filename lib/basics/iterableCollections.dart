/* commented to hide errors
static class IterableCollection() {

    static Iterable<String> IterateList() {
        Iterable<String> iterable = const ['Salad', 'Popcorn', 'Toast'];
        print('The first element is ${iterable.first}');
        print('The last element is ${iterable.last}');
        return iterable;
    }

    // Using firstWhere()
    void testFirstWhere () {
        const items = ['Salad', 'Popcorn', 'Toast', 'Lasagne'];

        // You can find with a simple expression:
        var foundItem1 = items.firstWhere((item) => item.length > 5);
        print(foundItem1);

        // Or try using a function block:
        var foundItem2 = items.firstWhere((item) {
            return item.length > 5;
        });
        print(foundItem2);

        // Or even pass in a function reference:
        var foundItem3 = items.firstWhere(predicate);
        print(foundItem3);

        // You can also use an `orElse` function in case no value is found!
        var foundItem4 = items.firstWhere(
            (item) => item.length > 10,
            orElse: () => 'None!',
        );
        print(foundItem4);
    }

    // Your goal is to implement the predicate for singleWhere() that satisfies the following conditions:
        // The element contains the character 'a'.
        // The element starts with the character 'M'.
    void testSingleWhere (Iterable<String> items) {
        return items.singleWhere((item) {
            return item.contains('a') && item.startsWith('M');
        });

        // or original
        // return items.singleWhere(
        //   (element) => element.startsWith('M') && element.contains('a'));
    }


    // Checking conditions
    void checkingConditions () {
        // BAD example
        // for (final item in items) {
        //     if (item.length < 5) {
        //         return false;
        //     }
        // }
        // return true;

        // OR
        return items.every((item) => item.length >= 5);
    }

    // Verify that an Iterable satisfies a condition
    // The following exercise provides practice using the any() and every() methods, described in the previous example. In this case, you work with a group of users, represented by User objects that have the member field age.
        // Use any() and every() to implement two functions:
        // Part 1: Implement anyUserUnder18().
        // Return true if at least one user is 17 or younger.
        // Part 2: Implement everyUserOver13().
        // Return true if all users are 14 or older.

    void testIteration () {
        bool anyUserUnder18(Iterable<User> users) {
            return users.any((item) => item.age < 18);
        }

        bool everyUserOver13(Iterable<User> users) {
            return users.every((item) => item.age >13);
        }
    }

    // Filtering
    // var evenNumbers = numbers.where((number) => number.isEven);

    // Filtering elements from a list
        // The following exercise provides practice using the where() method with the class User from the previous exercise.

        // Use where() to implement two functions:

        // Part 1: Implement filterOutUnder21().
        // Return an Iterable containing all users of age 21 or more.
        // Part 2: Implement findShortNamed().
        // Return an Iterable containing all users with names of length 3 or less.
    void testFiltering () {
        Iterable<User> filterOutUnder21(Iterable<User> users) {
           return users.where((user) => user.age >= 21);
        }

        Iterable<User> findShortNamed(Iterable<User> users) {
            return users.where((user) => user.name.length <= 3);
        }
    }

    // Mapping
    // Iterable<int> output = numbers.map((number) => number * 10);

    // In this exercise, your code takes an Iterable of User, and you need to return an Iterable that contains strings containing each user’s name and age.
    // Each string in the Iterable must follow this format: '{name} is {age}'—for example 'Alice is 21'.

    //     Part 1: Implement parseEmailAddresses().

    // Write the function parseEmailAddresses(), which takes an Iterable<String> containing email addresses, and returns an Iterable<EmailAddress>.
    // Use the method map() to map from a String to EmailAddress.
    // Create the EmailAddress objects using the constructor EmailAddress(String).
    // Part 2: Implement anyInvalidEmailAddress().

    // Write the function anyInvalidEmailAddress(), which takes an Iterable<EmailAddress> and returns true if any EmailAddress in the Iterable isn’t valid.
    // Use the method any() together with the provided function isValidEmailAddress().
    // Part 3: Implement validEmailAddresses().

    // Write the function validEmailAddresses(), which takes an Iterable<EmailAddress> and returns another Iterable<EmailAddress> containing only valid addresses.
    // Use the method where() to filter the Iterable<EmailAddress>.
    // Use the provided function isValidEmailAddress() to evaluate whether an EmailAddress is valid.

    Iterable<String> getNameAndAges(Iterable<User> users) {
        return users.map((user) => '${user.name} is ${user.age}');
    }

    Iterable<EmailAddress> parseEmailAddresses(Iterable<String> strings) {
        return strings.map((email) => EmailAddress(email));
    }

    bool anyInvalidEmailAddress(Iterable<EmailAddress> emails) {
        return emails.any((email) => !isValidEmailAddress(email));
    }

    Iterable<EmailAddress> validEmailAddresses(Iterable<EmailAddress> emails) {
        return emails.where((email) => isValidEmailAddress(email));
    }



}

class User {
  String name;
  int age;

  User(
    this.name,
    this.age,
  );
}

class EmailAddress {
  final String address;

  EmailAddress(this.address);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is EmailAddress &&
              address == other.address;

  @override
  int get hashCode => address.hashCode;

  @override
  String toString() {
    return 'EmailAddress{address: $address}';
  }
}

*/